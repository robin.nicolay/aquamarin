/*
 * Copyright (C) 2004 Derek James and Philip Tucker
 * 
 * This file is part of ANJI (Another NEAT Java Implementation).
 * 
 * ANJI is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307 USA
 * 
 * created by Philip Tucker on Feb 16, 2003
 */
package com.anji.neat;

import com.anji.integration.LogEventListener;
import com.anji.integration.PersistenceEventListener;
import com.anji.integration.PresentationEventListener;
import com.anji.persistence.Persistence;
import com.anji.run.Run;
import com.anji.util.Configurable;
import com.anji.util.Properties;
import com.anji.util.Reset;
import org.apache.log4j.Logger;
import org.jgap.BulkFitnessFunction;
import org.jgap.Chromosome;
import org.jgap.ChromosomeMaterial;
import org.jgap.Genotype;
import org.jgap.event.GeneticEvent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Configures and performs an ANJI evolutionary run.
 *
 * @author Philip Tucker
 */
public class Evolver implements Configurable {

    private static Logger logger = Logger.getLogger(Evolver.class);

    /**
     * properties key, # generations in run
     */
    public static final String NUM_GENERATIONS_KEY = "num.generations";

    /**
     * properties key, fitness function class
     */
    public static final String FITNESS_FUNCTION_CLASS_KEY = "fitness_function";

    private static final String FITNESS_THRESHOLD_KEY = "fitness.threshold";

    private static final String RESET_KEY = "run.reset";

    /**
     * properties key, target fitness value - after reaching this run will halt
     */
    public static final String FITNESS_TARGET_KEY = "fitness.target";

    private NeatConfiguration config = null;

    private Chromosome champ = null;

    private Genotype genotype = null;

    private int numEvolutions = 0; // Vor Aquamarin als Limit genutzt.

    private double targetFitness = 0.0d;

    private double thresholdFitness = 0.0d;

    private int maxFitness = 0;

    private Persistence db = null;

    private Date aquamarinRunStartDate;
    private DateFormat aquamarinFmt;
    private int aquamarinGenerationOfFirstSolution;
    private double aquamarinAdjustedFitness;
    private int aquamarinGeneration = 0;
    Date aquamarinGenerationStartDate;
    private String name;

    /**
     * ctor; must call <code>init()</code> before using this object
     */
    public Evolver() {
        super();
    }

    /**
     * Construct new evolver with given properties. See <a href=" {@docRoot}/params.htm"
     * target="anji_params">Parameter Details </a> for specific property settings.
     *
     * @see com.anji.util.Configurable#init(com.anji.util.Properties)
     */
    public void init(Properties props) throws Exception {
        boolean doReset = props.getBooleanProperty(RESET_KEY, false);
        if (doReset) {
            logger.warn("Resetting previous run !!!");
            Reset resetter = new Reset(props);
            resetter.setUserInteraction(false);
            resetter.reset();
        }

        config = new NeatConfiguration(props);

        // peristence
        db = (Persistence) props.singletonObjectProperty(Persistence.PERSISTENCE_CLASS_KEY);

        numEvolutions = props.getIntProperty(NUM_GENERATIONS_KEY);
        targetFitness = props.getDoubleProperty(FITNESS_TARGET_KEY, 1.0d);
        thresholdFitness = props.getDoubleProperty(FITNESS_THRESHOLD_KEY, targetFitness);

        //
        // event listeners
        //

        // run
        // TODO - hibernate
        Run run = (Run) props.singletonObjectProperty(Run.class);
        db.startRun(run.getName());
        config.getEventManager().addEventListener(GeneticEvent.GENOTYPE_EVALUATED_EVENT, run);

        // logging
        LogEventListener logListener = new LogEventListener(config);
        config.getEventManager().addEventListener(GeneticEvent.GENOTYPE_EVOLVED_EVENT, logListener);
        config.getEventManager()
                .addEventListener(GeneticEvent.GENOTYPE_EVALUATED_EVENT, logListener);

        // persistence
        PersistenceEventListener dbListener = new PersistenceEventListener(config, run);
        dbListener.init(props);
        config.getEventManager().addEventListener(
                GeneticEvent.GENOTYPE_START_GENETIC_OPERATORS_EVENT, dbListener);
        config.getEventManager().addEventListener(
                GeneticEvent.GENOTYPE_FINISH_GENETIC_OPERATORS_EVENT, dbListener);
        config.getEventManager().addEventListener(GeneticEvent.GENOTYPE_EVALUATED_EVENT, dbListener);

        // presentation
        PresentationEventListener presListener = new PresentationEventListener(run);
        presListener.init(props);
        config.getEventManager().addEventListener(GeneticEvent.GENOTYPE_EVALUATED_EVENT,
                presListener);
        config.getEventManager().addEventListener(GeneticEvent.RUN_COMPLETED_EVENT, presListener);

        // fitness function
        BulkFitnessFunction fitnessFunc = (BulkFitnessFunction) props
                .singletonObjectProperty(FITNESS_FUNCTION_CLASS_KEY);
        config.setBulkFitnessFunction(fitnessFunc);
        maxFitness = fitnessFunc.getMaxFitnessValue();

        // load population, either from previous run or random
        genotype = db.loadGenotype(config);
        if (genotype != null)
            logger.info("genotype from previous run");
        else {
            genotype = Genotype.randomInitialGenotype(config);
            logger.info("random genotype");
        }

    }

    /**
     * command line usage
     */
    private static void usage() {
        System.err.println("usage: <cmd> <properties-file>");
    }

    /**
     * Perform a single run.
     * F�r das Aquamarin-Projekt in 4 Funktionen unterteilt.
     *
     * @throws Exception
     */
    public Evolver run() throws Exception { // Aquamarin-�nderung: Typ auf 'Evolver' ge�ndert.
        // run start time
        aquamarinRunStartDate = Calendar.getInstance().getTime();
        logger.info("Run: start");
        aquamarinFmt = new SimpleDateFormat("HH:mm:ss");

        // initialize result data
        aquamarinGenerationOfFirstSolution = -1;
        champ = genotype.getFittestChromosome();
        aquamarinAdjustedFitness = (maxFitness > 0 ? champ.getFitnessValue() / maxFitness : champ
                .getFitnessValue());

        return this;
    }

    /**
     * Erzeugen einer neuen Generation.
     * Urspr�nglich in run().
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Chromosome> nextGeneration() {
        // generation start time
        aquamarinGenerationStartDate = Calendar.getInstance().getTime();
        logger.info("Generation " + aquamarinGeneration++ + ": start");

        return genotype.getChromosomes();
    }

    /**
     * Entwickeln einer Generation.
     * Urpsr�nglich in run().
     */
    public void evolveGeneration() {
        // next generation
        genotype.evolve();

        // result data
        champ = genotype.getFittestChromosome();
        System.out.println("Evolver(" + name + ").evolveGeneration.champ.size: " + champ.getAlleles().size());
        aquamarinAdjustedFitness = (maxFitness > 0 ? (double) champ.getFitnessValue() / maxFitness : champ
                .getFitnessValue());
        if (aquamarinAdjustedFitness >= thresholdFitness && aquamarinGenerationOfFirstSolution == -1)
            aquamarinGenerationOfFirstSolution = aquamarinGeneration;

        // generation finish
        Date generationEndDate = Calendar.getInstance().getTime();
        long durationMillis = generationEndDate.getTime() - aquamarinGenerationStartDate.getTime();
        logger.info("Generation " + aquamarinGeneration + ": end [" + aquamarinFmt.format(aquamarinGenerationStartDate)
                + " - " + aquamarinFmt.format(generationEndDate) + "] [" + durationMillis + "]");

    }

    /**
     * Korrektes Beenden des Evolvers nach der Simulation.
     * Urspr�nglich in run().
     */
    public void finishSimulation() {
        // run finish
        config.getEventManager().fireGeneticEvent(
                new GeneticEvent(GeneticEvent.RUN_COMPLETED_EVENT, genotype));
        logConclusion(aquamarinGenerationOfFirstSolution, champ);
        Date runEndDate = Calendar.getInstance().getTime();
        long durationMillis = runEndDate.getTime() - aquamarinRunStartDate.getTime();
        logger.info("Run: end [" + aquamarinFmt.format(aquamarinRunStartDate) + " - " + aquamarinFmt.format(runEndDate)
                + "] [" + durationMillis + "]");
    }

    /**
     * Log summary data of run including generation in which the first solution occurred, and the
     * champion of the final generation.
     *
     * @param generationOfFirstSolution
     * @param champ
     */
    private static void logConclusion(int generationOfFirstSolution, Chromosome champ) {
        logger.info("generation of first solution == " + generationOfFirstSolution);
        logger.info("champ # connections == "
                + NeatChromosomeUtility.getConnectionList(champ.getAlleles()).size());
        logger.info("champ # hidden nodes == "
                + NeatChromosomeUtility.getNeuronList(champ.getAlleles(), NeuronType.HIDDEN).size());
    }

    /**
     * Main program used to perform an evolutionary run.
     *
     * @throws Throwable
     */
    public static Evolver instance(Properties props) throws Throwable { // Aquamarin-�nderung: 'static' entfernt
        try {
            Evolver evolver = new Evolver();
            evolver.init(props);
            // evolver.run();
            // System.exit( 0 );
            return evolver.run();
        } catch (Throwable th) {
            logger.error("", th);
            throw th;
        }
    }

    /**
     * @return champion of last generation
     */
    public Chromosome getChamp() {
        return champ;
    }

    /**
     * Fitness of current champ, 0 ... 1
     *
     * @return maximum fitness value
     */
    public double getChampAdjustedFitness() {
        return (champ == null) ? 0d : (double) champ.getFitnessValue()
                / config.getBulkFitnessFunction().getMaxFitnessValue();
    }

    /**
     * @return target fitness value, 0 ... 1
     */
    public double getTargetFitness() {
        return targetFitness;
    }

    /**
     * @return threshold fitness value, 0 ... 1
     */
    public double getThresholdFitness() {
        return thresholdFitness;
    }

    /**
     * Aquamarin Projekt
     * Exports a Chromosome from the current Genotype. The numerical parameter is the Chromosome's number which to export from the simulation.
     * If 0 is given as parameter, the fittest Chromosome is exported. If the parameter is greater then the count of the available Chromosomes,
     * the fittest one is exported.
     *
     * @param index
     * @return
     */
    public ChromosomeMaterial exportChromosome(int index) {
        if (index == 0) {
            return genotype.getFittestChromosome().cloneMaterial();
        } else {
            List<Chromosome> chromosomes = genotype.getChromosomes();
            if (index > chromosomes.size()) {
                return genotype.getFittestChromosome().cloneMaterial();
            } else {
                Chromosome chrom = (Chromosome) genotype.getChromosomes().get(index);
                return chrom.cloneMaterial();
            }
        }
    }

    /**
     * Aquamarin Projekt
     * Imports the given ChromosomeMaterial to the Chromosome with the number given as parameter. If 0 is given or the number is greater
     * then the count of the available Chromosomes, the ChromosomeMaterial is assigned to a random Chromosome.
     *
     * @param chromMat
     * @param index
     */
    public void importChromosome(ChromosomeMaterial chromMat, int index) {
        List<Chromosome> chroms = genotype.getChromosomes();
        if (index == 0 || index > chroms.size()) {
            chroms.get(index).importMaterial(chromMat);
        } else {
            index = (int) (Math.random() * chroms.size());
            chroms.get(index).importMaterial(chromMat);
        }
    }

    /**
     * Aquamarin Projekt
     * It is advised to use importChromosome(ChromosomeMaterial chromMat, int index) instead.
     * Imports the given ChromosomeMaterial to the Chromosome with the number given as parameter. If 0 is given or the number is greater
     * then the count of the available Chromosomes, the ChromosomeMaterial is assigned to a random Chromosome.
     *
     * @param chromMat
     * @param index
     */
    public void importChromosome(Chromosome chrom, int index) {
        importChromosome(chrom.cloneMaterial(), index);
    }

    public void setName(String name) {
        this.name = name;
    }
}
