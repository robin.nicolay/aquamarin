package uni.hro.draw;

/**
 * 
 */

import uni.hro.model.Kraken;

import java.awt.*;
import java.awt.geom.*;

/**
 * @author Joerg
 *
 *         Diese Klasse bestimmt, wie ein toter Kraken gezeichnet wird.
 */

public class DeadKrakenDrawer {

	/**
	 * Diese Methode bestimmt, wie ein toter Kraken gezeichnet wird.
	 * 
	 * @param graphics
	 *            Grafik des AquariumJPanel
	 * @param kraken
	 *            Der zu zeichnende Kraken
	 */
	public static void draw(Graphics g, Kraken kraken)

	{

		Graphics2D g2 = (Graphics2D) g;

		/**
		 * Anpassen der Position, damit der Kraken genau in der Hit-Box
		 * gezeichnet wird
		 */
		int positionX = (int) (kraken.getX() - 9);
		int positionY = (int) (kraken.getY() - 23);

		/**
		 * SizefactorX / -Y bestimmen die Zeichengroesse des Kraken.
		 */
		double sizefactorX = 0.4;
		double sizefactorY = 0.4;
		Color color = kraken.getCurrentColor();

		/**
		 * Verschiebung des Objekts
		 */
		g2.translate(positionX, positionY);

		/**
		 * Usprungszustand des Objekts
		 */
		AffineTransform atOriginal = g2.getTransform();

		/**
		 * Skalierung des Objekts
		 */
		AffineTransform at = new AffineTransform();
		at.setToScale(sizefactorX, sizefactorY);

		/**
		 * Transformationen anwenden
		 */
		g2.transform(at);

		/**
		 * Antialiasing aktiviert
		 */
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		/**
		 * Arm5
		 */
		int[] xPoints5 = { 25, 50 };
		int[] yPoints5 = { 25, 70 };
		int nPoints5 = 2;
		g2.setColor(Color.WHITE);
		g2.fillPolygon(xPoints5, yPoints5, nPoints5);

		/**
		 * Arm4
		 */
		int[] xPoints4 = { 35, 45 };
		int[] yPoints4 = { 25, 72 };
		int nPoints4 = 2;

		g2.setColor(Color.WHITE);
		g2.drawPolygon(xPoints4, yPoints4, nPoints4);

		/**
		 * Arm 3
		 */
		int[] xPoints = { 30, 40 };
		int[] yPoints = { 40, 60 };
		int nPoints = 2;

		g2.setColor(Color.WHITE);
		g2.drawPolygon(xPoints, yPoints, nPoints);

		/**
		 * Arm1
		 */
		int[] xPoints2 = { 19, -5 };
		int[] yPoints2 = { 25, 70 };
		int nPoints2 = 2;

		g2.setColor(Color.WHITE);
		g2.drawPolygon(xPoints2, yPoints2, nPoints2);

		/**
		 * Arm6
		 */
		int[] xPoints6 = { 17, 7 };
		int[] yPoints6 = { 25, 70 };
		int nPoints6 = 2;

		g2.setColor(Color.WHITE);
		g2.drawPolygon(xPoints6, yPoints6, nPoints6);

		/**
		 * Arm2
		 */
		int[] xPoints3 = { 30, 25, };
		int[] yPoints3 = { 25, 80, };
		int nPoints3 = 2;

		g2.setColor(Color.WHITE);
		g2.drawPolygon(xPoints3, yPoints3, nPoints3);

		/**
		 * Unterkiefer
		 */

		g2.setColor(color.white);
		g2.fillOval(15, 30, 20, 20);
		g2.setColor(Color.BLACK);
		g2.drawOval(15, 30, 20, 20);

		/**
		 * Oberer Schaedel
		 */
		g2.setColor(color.white);
		g2.fillOval(5, 10, 40, 30);
		g2.setColor(Color.BLACK);
		g2.drawOval(5, 10, 40, 30);

		/**
		 * Auge1
		 */
		g2.setColor(color.BLACK);
		g2.fillOval(15, 23, 7, 7);

		/**
		 * Auge2
		 */
		g2.setColor(color.BLACK);
		g2.fillOval(30, 23, 7, 7);

		/**
		 * Nasenhoehle
		 */
		g2.setColor(color.BLACK);
		g2.fillOval(24, 32, 5, 5);

		/**
		 * Mund
		 */
		g2.setColor(color.BLACK);
		g2.fillOval(23, 41, 7, 7);

		/**
		 * Geisterschleier
		 * 
		 */
		int[] xPoints7 = { 10, 0, 10, 20, 30, 40, 50, 40 };
		int[] yPoints7 = { 30, 70, 90, 80, 100, 80, 85, 30 };
		int nPoints7 = 8;

		g2.setColor(new Color(255, 255, 255, 120));
		g2.fillPolygon(xPoints7, yPoints7, nPoints7);

		/**
		 * Transformation zuruecksetzen
		 */
		g2.setTransform(atOriginal);

		/**
		 * Reset Verschiebung des Objekts
		 */
		g2.translate(-positionX, -positionY);

	}
}