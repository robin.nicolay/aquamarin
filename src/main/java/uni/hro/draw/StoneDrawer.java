package uni.hro.draw;

/**
 * 
 */

import uni.hro.model.Stone;

import java.awt.*;
import java.awt.geom.*;
import java.util.Random;

/**
 * @author Joerg
 *
 */

public class StoneDrawer {

	/**
	 * Diese Methode bestimmt, wie ein Stein gezeichnet wird.
	 * 
	 * @param graphics
	 *            Grafik des AquariumJPanel
	 * @param stone
	 *            Der zu zeichnende Stein
	 */
	public static void draw(Graphics g, Stone stone) {

		Graphics2D g2 = (Graphics2D) g;

		/**
		 * Anpassen der Position, damit der Stein genau in der Hit-Box
		 * gezeichnet wird
		 */
		int positionX = (int) stone.getX() - 40;
		int positionY = (int) stone.getY() + 23;

		int[] xPoints = stone.getxGeometriePoints();
		int[] yPoints = stone.getyGeometriePoints();

		/**
		 * SizefactorX / -Y bestimmen die Zeichengroesse des Kraken.
		 */
		double sizefactorX = 0.8;
		double sizefactorY = 0.53;

		Color color = stone.getCurrentColor();

		/**
		 * Verschiebung des Objekts
		 */
		g2.translate(positionX, positionY);

		/**
		 * Usprungszustand des Objekts
		 */
		AffineTransform atOriginal = g2.getTransform();

		/**
		 * Skalierung des Objekts
		 */
		AffineTransform at = new AffineTransform();
		at.setToScale(sizefactorX, sizefactorY);
		if (sizefactorX < 0) {
			g2.translate(-100 * sizefactorX, 0);
		}
		/**
		 * Transformationen anwenden
		 */
		g2.transform(at);

		/**
		 * Antialiasing aktiviert
		 */
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		/**
		 * Steinpunkte Malen 
		 */
		int[] xPoints2 = xPoints;
		int[] yPoints2 = yPoints;

		int nPoints2 = xPoints.length;
		g2.setColor(color);
		g2.fillPolygon(xPoints2, yPoints2, nPoints2);
		g2.setColor(Color.BLACK);
		g2.drawPolygon(xPoints2, yPoints2, nPoints2);
		/**
		 * Transformation zuruecksetzen
		 */
		g2.setTransform(atOriginal);

		/**
		 * Reset Verschiebung des Objekts
		 */
		g2.translate(-positionX, -positionY);

	}

	/**
	 * Erzeugt zufaellige X-Werte zum Zeichnen des Steins.
	 */
	public int[] createRandomizedGeometrieValuesX() {
		int[] xPoints = { 0, -12, 0, 10, 40, 50, 60, 70, 100, 112, 100 };
		return xPoints;
	}

	/**
	 * Erzeugt zufaellige Y-Werte zum Zeichnen des Steins.
	 */
	public int[] createRandomizedGeometrieValuesY() {
		int[] yPoints = { 0, -20 + generateRandom(-20, 20), -40 + generateRandom(-20, 20),
				-60 + generateRandom(-20, 20), -80 + generateRandom(-5, 2), -100 + generateRandom(0, 20),
				-80 + generateRandom(-5, 20), -60 + generateRandom(-20, 20), -40 + generateRandom(-20, 20),
				-20 + generateRandom(-20, 20), 0 };

		return yPoints;
	}

	/**
	 * Erzeugt eine Integer-Zufallszahl zwischen min und max.
	 * 
	 * @param min
	 * @param max
	 * @return
	 */
	public static int generateRandom(int min, int max) {
		Random rn = new Random();
		int randomvalue = rn.nextInt(max - min + 1) + min;
		return randomvalue;
	}
}