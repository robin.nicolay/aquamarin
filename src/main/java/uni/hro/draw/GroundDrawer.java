package uni.hro.draw;

/**
 * 
 */

import java.awt.*;
import java.awt.geom.*;
import java.util.Random;

/**
 * @author Joerg
 *
 *         Diese Klasse bestimmt, wie der Boden gezeichnet wird.
 */

public class GroundDrawer {
	static int[] xPoints2;
	static int[] yPoints2;

	/**
	 * Diese Methode bestimmt, wie der Boden gezeichnet wird.
	 * 
	 * @param g
	 *            Die Grafik des AquariumJPanel
	 * @param screenResolutionX
	 *            Groesse der Welt in X-Richtung
	 * @param screenResolutionY
	 *            Groesse der Welt in Y-Richtung
	 * @param color
	 *            Bodenfarbe
	 * @param sizefactorX
	 *            Vergroessert / Vergkleinert den Boden in X-Richtung
	 * @param sizefactorY
	 *            Vergroessert / Vergkleinert den Boden in Y-Richtung
	 */
	public static void draw(Graphics g, int screenResolutionX, int screenResolutionY, Color color, double sizefactorX,
			double sizefactorY) {

		Graphics2D g2 = (Graphics2D) g;

		/**
		 * Verschiebung des Objekts
		 */
		g2.translate(0, screenResolutionY);

		/**
		 * Usprungszustand des Objekts
		 */
		AffineTransform atOriginal = g2.getTransform();

		/**
		 * Skalierung des Fisches
		 */
		AffineTransform at = new AffineTransform();
		at.setToScale(sizefactorX, sizefactorY);
		if (sizefactorX < 0) {
			g2.translate(-100 * sizefactorX, 0);
		}
		/**
		 * Transformationen anwenden
		 */
		g2.transform(at);

		/**
		 * Boden zeichnen
		 */

		if (xPoints2 == null) {
			xPoints2 = createRandomizedGeometrieValuesX(screenResolutionX);
			yPoints2 = createRandomizedGeometrieValuesY(screenResolutionX);
		}

		int nPoints2 = xPoints2.length;
		g2.setColor(color);
		g2.fillPolygon(xPoints2, yPoints2, nPoints2);
		g2.setColor(Color.BLACK);
		g2.drawPolygon(xPoints2, yPoints2, nPoints2);

		/**
		 * Reset Verschiebung des Objekts
		 */
		g2.translate(-0, -screenResolutionY);

	}

	/**
	 * Erzeugt zufaellige X-Werte zum Zeichnen des Bodens.
	 * 
	 * @param screenResolutionX
	 *            Groesse der Welt in X-Richtung
	 * @return
	 */
	public static int[] createRandomizedGeometrieValuesX(int screenResolutionX) {

		int stepSize = 20;
		int lastXpoint = screenResolutionX / stepSize + 3;
		int[] xPoints = new int[lastXpoint];

		int currentGroundlength = 0;
		int iterator = 2;

		xPoints[0] = currentGroundlength;
		xPoints[1] = currentGroundlength;
		while (iterator < xPoints.length) {
			currentGroundlength = currentGroundlength + stepSize;
			xPoints[iterator] = currentGroundlength;
			iterator++;
		}

		xPoints[lastXpoint - 2] = currentGroundlength;
		xPoints[lastXpoint - 1] = currentGroundlength;
		return xPoints;
	}

	/**
	 * Erzeugt zufaellige Y-Werte zum Zeichnen des Bodens.
	 * 
	 * @param screenResolutionX
	 *            Groesse der Welt in X-Richtung
	 * @return
	 */
	public static int[] createRandomizedGeometrieValuesY(int screenResolutionX) {

		int stepSize = 20;
		int lastYpoint = screenResolutionX / stepSize + 3;
		int[] yPoints = new int[lastYpoint];

		int iterator = 2;

		yPoints[0] = 0;
		yPoints[1] = -50;
		while (iterator < yPoints.length) {

			yPoints[iterator] = -50 + generateRandom(5, 10);
			iterator++;
		}

		yPoints[lastYpoint - 2] = -50;
		yPoints[lastYpoint - 1] = 0;

		return yPoints;
	}

	/**
	 * Erzeugt eine Integer-Zufallszahl zwischen min und max.
	 * 
	 * @param min
	 * @param max
	 * @return
	 */
	public static int generateRandom(int min, int max) {
		Random rn = new Random();
		int randomvalue = rn.nextInt(max - min + 1) + min;
		return randomvalue;
	}
}