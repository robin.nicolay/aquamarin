/**
 * 
 */
package uni.hro.draw.neuronalnetdrawer;

import java.awt.Color;
import java.util.ArrayList;

/**
 * @author Columbus
 *
 */
public class Neuron {

	private long id;
	private int x;
	private int y;
	private int size = 20;
	private int centerX;
	private int centerY;
	private Color color = Color.BLACK;
	private ArrayList<Connection> connList = new ArrayList<Connection>();
	private boolean highlighted =false;
	
	public Neuron(long id, int x, int y) {
		this.x = x;
		this.y = y;
		this.id = id;
		this.centerX = x + size / 2;
		this.centerY = y + size / 2;

	}

	public void addToConnList(Connection connection) {
		this.connList.add(connection);
	}

	/**
	 * Setter und Getter
	 */

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getCenterX() {
		return centerX;
	}

	public void setCenterX(int centerX) {
		this.centerX = centerX;
	}

	public int getCenterY() {
		return centerY;
	}

	public void setCenterY(int centerY) {
		this.centerY = centerY;
	}

	public ArrayList<Connection> getConnList() {
		return connList;
	}

	public void setConnList(ArrayList<Connection> connList) {
		this.connList = connList;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public boolean isHighlighted() {
		return highlighted;
	}

	public void setHighlighted(boolean highlighted) {
		this.highlighted = highlighted;
	}

}
