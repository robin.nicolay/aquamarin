package uni.hro.server;

import java.awt.Color;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.InetAddress;

import net.miginfocom.swing.MigLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.ComponentOrientation;
import java.awt.Font;
import java.util.Properties;

/**
 *
 * @author Johann
 *
 *	GUI of the server.
 *
 */
public class ServerGui extends JFrame {

	private static GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
	private JPanel contentPane;
	private JTextField ClientField;
	private JButton startButton;
	private JButton exitButton;
	private JScrollPane scrollPane;
	private JPanel panel;
	private JPanel numberPanel;
	private JLabel lblNumberOfClients;
	private JLabel numLabel;

	private int noC = 0; // Number of Clients connected

	private Integer[] numbers = new Integer[24];
	private Integer[] numbers2 = new Integer[13];

	private JLabel lblFische;
	private JComboBox fischBox;
	private JLabel lblKraken;
	private JComboBox krakenBox;
	private JLabel lblSteine;
	private JComboBox steinBox;
	private JLabel lblPflanzen;
	private JComboBox pflanzenBox;

	private JRadioButton rdbtnVollbild;
	private JRadioButton rdbtnFenster;
	private JLabel lblX;
	private JTextField textField_X;
	private JLabel lblY;
	private JTextField textField_Y;
	private JCheckBox resetBox;

	private Settings lastRunSettings;
	
	/**
	 * Create the frame.
	 */
	public ServerGui() {

		// Loading settings from last run.
		if (Settings.exists())
			lastRunSettings = Settings.load();
		else
			lastRunSettings = new Settings(0, 15, 2, 1, 1, 480, 360);
		
		setMinimumSize(new Dimension(479, 319));
		setTitle("Aquarium Server");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 340);
		numbers = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 50, 100, 200};	//numbers of fish and kraken
		numbers2 = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20};	// numbers of plants and stones
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[150px:n:150px][150px:n:150px,grow][150px:n:150px,grow]", "[30px:n:30px][30px:n:30px,grow][50px:n:50px,grow][][][][][]"));

		scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		contentPane.add(scrollPane, "cell 0 0 3 2,grow");

		panel = new JPanel();
		scrollPane.setViewportView(panel);

		ClientField = addClient();

		numberPanel = new JPanel();
		contentPane.add(numberPanel, "cell 0 2 3 1,alignx center,aligny center");

		lblNumberOfClients = new JLabel("Number of Clients:");
		lblNumberOfClients.setFont(new Font("Tahoma", Font.BOLD, 16));
		numberPanel.add(lblNumberOfClients);

		numLabel = new JLabel("0");
		numLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		numberPanel.add(numLabel);

		exitButton = new JButton("Exit");
		exitButton.setPreferredSize(new Dimension(144, 23));
		exitButton.setMinimumSize(new Dimension(144, 23));
		exitButton.setMaximumSize(new Dimension(144, 23));
		contentPane.add(exitButton, "cell 0 3,alignx center,aligny center");

		startButton = new JButton("Start");
		startButton.setMaximumSize(new Dimension(144, 23));
		startButton.setMinimumSize(new Dimension(144, 23));
		startButton.setPreferredSize(new Dimension(144, 23));
		contentPane.add(startButton, "cell 2 3,alignx center,aligny center");

		fischBox = new JComboBox(numbers);
		contentPane.add(fischBox, "flowx,cell 0 4");
		fischBox.setSelectedIndex(lastRunSettings.getNumFish());

		lblFische = new JLabel("Fische");
		contentPane.add(lblFische, "cell 0 4");

		rdbtnVollbild = new JRadioButton("Vollbild");
		rdbtnVollbild.setSelected(true);
		contentPane.add(rdbtnVollbild, "cell 1 4,alignx right");

		rdbtnFenster = new JRadioButton("Fenster");
		contentPane.add(rdbtnFenster, "flowx,cell 2 4");

		ButtonGroup group = new ButtonGroup();
		group.add(rdbtnVollbild);
		group.add(rdbtnFenster);
		rdbtnVollbild.addActionListener(new fullscreenListener());
		rdbtnFenster.addActionListener(new windowListener());

		krakenBox = new JComboBox(numbers);
		contentPane.add(krakenBox, "flowx,cell 0 5");
		krakenBox.setSelectedIndex(lastRunSettings.getNumKraken());

		lblKraken = new JLabel("Kraken");
		contentPane.add(lblKraken, "cell 0 5");

		lblX = new JLabel("x:");
		contentPane.add(lblX, "flowx,cell 2 5");

		steinBox = new JComboBox(numbers2);
		contentPane.add(steinBox, "flowx,cell 0 6");
		steinBox.setSelectedIndex(lastRunSettings.getNumStone());

		lblSteine = new JLabel("Steine");
		contentPane.add(lblSteine, "cell 0 6");

		pflanzenBox = new JComboBox(numbers2);
		contentPane.add(pflanzenBox, "flowx,cell 0 7");
		pflanzenBox.setSelectedIndex(lastRunSettings.getNumPlant());

		lblPflanzen = new JLabel("Pflanzen");
		contentPane.add(lblPflanzen, "cell 0 7");

		textField_X = new JTextField();
		textField_X.setText("" + lastRunSettings.getX());
		contentPane.add(textField_X, "cell 2 5");
		textField_X.setColumns(6);
		textField_X.setEnabled(false);

		lblY = new JLabel(" y:");
		contentPane.add(lblY, "cell 2 5");

		textField_Y = new JTextField();
		textField_Y.setText("" + lastRunSettings.getY());
		contentPane.add(textField_Y, "cell 2 5");
		textField_Y.setColumns(6);
		textField_Y.setEnabled(false);
		
		resetBox = new JCheckBox("Reset");
		contentPane.add(resetBox, "cell 2 7");
		resetBox.setSelected(true);
		resetBox.addActionListener(new resetListener());

		// make it fullscreen, if on pi
		String os = "os.name";

		Properties prop = System.getProperties( );
		System.out.println( prop.getProperty( os ) );

		// if this is run on an Raspberry, it goes to fullscreen and only to a tiny window if not
		if ("4.4.50-v7+".equals(System.getProperty("os.version"))) {
			setResizable(false);
			setUndecorated(true); // deletes the frame decoration (the upper bar of the frame)
			gd.setFullScreenWindow(this); // GraphicsEnvironment is essential to create a fullscreen application in unix

			// disable selection for fullscreen or windowed-mode (only fullscreen)
			rdbtnVollbild.setEnabled(false);
			rdbtnVollbild.setSelected(true);
			rdbtnFenster.setVisible(false);
			lblX.setVisible(false);
			lblY.setVisible(false);
			textField_X.setVisible(false);
			textField_Y.setVisible(false);
		} else {
			
			setResizable(true);
			setUndecorated(false);
			
			setMinimumSize(new Dimension(515, 375));
			
		}
		setVisible(true);

		// Listener for the buttons.
		exitButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.exit(0);
			}
		});

		connectClient("localhost");
	}	// end of constructor


	/** getter **/
	public int getXForSimulation() {
		if (rdbtnVollbild.isSelected()) {
			// return x size of the current screen resolution (for fullscreen)
			return gd.getDisplayMode().getWidth();
		}
		else {
			return Integer.parseInt(textField_X.getText());
		}
	}

	public int getYForSimulation() {
		if (rdbtnVollbild.isSelected()) {
			// return y size of the current screen resolution (for fullscreen)
			return gd.getDisplayMode().getHeight();
		}
		else {
			return Integer.parseInt(textField_Y.getText());
		}
	}

	public int getNumberOfFishes() { return numbers[fischBox.getSelectedIndex()]; }
	public int getNumberOfKraken() { return numbers[krakenBox.getSelectedIndex()]; }
	public int getNumberOfPlants() { return numbers[pflanzenBox.getSelectedIndex()]; }
	public int getNumberOfStones() { return numbers[steinBox.getSelectedIndex()]; }

	public boolean getReset() {
		if (resetBox.isSelected()) return true;
		else return false;
	}
	
	/**
	 *
	 * @param ipAddress IP-Address of the connected client
	 */

	public void connectClient(String ipAddress) {
		noC++;
		ClientField.setText(noC + ": " + ipAddress);
		ClientField.setBackground(new Color(0, 255, 0));
		ClientField.setToolTipText(ipAddress);
		ClientField = addClient();
		numLabel.setText(""+noC);
	}

	private JTextField addClient() {
		panel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
		JTextField textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textField.setAlignmentY(1.0f);
		textField.setAlignmentX(1.0f);
		textField.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		textField.setColumns(11);
		panel.add(textField);
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setText("Waiting for Clients...");
		textField.setEnabled(false);
		textField.setEditable(false);
		panel.revalidate();
		panel.repaint();
		return textField;
	}

	public void storeCurrentSettings() {
		lastRunSettings.setNumFish(fischBox.getSelectedIndex());
		lastRunSettings.setNumKraken(krakenBox.getSelectedIndex());
		lastRunSettings.setNumPlant(pflanzenBox.getSelectedIndex());
		lastRunSettings.setNumStone(steinBox.getSelectedIndex());
		lastRunSettings.setX(Integer.parseInt(textField_X.getText()));
		lastRunSettings.setY(Integer.parseInt(textField_Y.getText()));
		lastRunSettings.store();
	}
	
	public void setStartListener(ActionListener l) { this.startButton.addActionListener(l);	}

	class fullscreenListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			textField_X.setEnabled(false);
			textField_Y.setEnabled(false);
		}
	}

	class windowListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			if (!resetBox.isSelected()) {
				textField_X.setEnabled(false);
				textField_Y.setEnabled(false);
			}
			else {
				textField_X.setEnabled(true);
				textField_Y.setEnabled(true);
			}
		}
	}
	
	class resetListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (!resetBox.isSelected()) {
				fischBox.setSelectedIndex(lastRunSettings.getNumFish());
				fischBox.setEnabled(false);
				krakenBox.setSelectedIndex(lastRunSettings.getNumKraken());
				krakenBox.setEnabled(false);
				steinBox.setSelectedIndex(lastRunSettings.getNumStone());
				steinBox.setEnabled(false);
				pflanzenBox.setSelectedIndex(lastRunSettings.getNumPlant());
				pflanzenBox.setEnabled(false);
				textField_X.setText("" + lastRunSettings.getX());
				textField_X.setEnabled(false);
				textField_Y.setText("" + lastRunSettings.getY());
				textField_Y.setEnabled(false);
			} else {
				fischBox.setEnabled(true);
				krakenBox.setEnabled(true);
				steinBox.setEnabled(true);
				pflanzenBox.setEnabled(true);
				if (rdbtnVollbild.isSelected()) {
					textField_X.setEnabled(false);
					textField_Y.setEnabled(false);
				}
				else {
					textField_X.setEnabled(true);
					textField_Y.setEnabled(true);
				}
			}
		}
	}
}