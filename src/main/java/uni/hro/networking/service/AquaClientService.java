package uni.hro.networking.service;

import com.sun.istack.internal.NotNull;
import de.mein.auth.data.IPayload;
import de.mein.auth.data.db.Certificate;
import de.mein.auth.jobs.Job;
import de.mein.auth.service.MeinAuthService;
import de.mein.auth.socket.process.val.MeinServicesPayload;
import de.mein.auth.socket.process.val.MeinValidationProcess;
import de.mein.auth.socket.process.val.Request;
import de.mein.auth.tools.N;
import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;
import uni.hro.client.ClientGui;
import uni.hro.model.Bucket;
import uni.hro.AquaStrings;
import uni.hro.networking.job.SendBucketJob;
import uni.hro.networking.job.StartSimulationJob;

import java.io.File;

/**
 * Created by xor on 5/12/17.
 */
public class AquaClientService extends AquaService {
    private final ClientGui gui;
    private Long serverId;
    private String serverServiceUuid;

    public AquaClientService(MeinAuthService meinAuthService, File serviceInstanceWorkingDirectory) {
        super(meinAuthService, serviceInstanceWorkingDirectory);
        this.gui = new ClientGui(this);
    }

    public Promise<Void, Void, Void> regAsClient(@NotNull String address, @NotNull int port, @NotNull int portCert) throws InterruptedException {
        DeferredObject<Void, Void, Void> deferred = new DeferredObject<>();
        // connect & register first
        Promise<MeinValidationProcess, Exception, Void> connected = meinAuthService.connect(null, address, port, portCert, true);
        connected.done(val -> N.r(() -> {
            // collect the two things we need to know to talk to the servers service:
            // serverId (stored on client side)
            serverId = val.getPartnerCertificate().getId().v();
            // serverServiceUuid (the "name" of the servers service (on servers side))
            // (server grants access to the service by default)
            Request<MeinServicesPayload> allowedRemoteServices = meinAuthService.getAllowedServices(serverId);
            allowedRemoteServices.done(remoteServicesPayload -> N.r(() -> {
                serverServiceUuid = remoteServicesPayload.getServices().get(0).getUuid().v();
                MeinServicesPayload allowedServices = meinAuthService.getAllowedServicesFor(val.getPartnerCertificate().getId().v());
                // ask for registration. when done trigger the deferred object
                Request regRequest = val.request(serverServiceUuid, AquaStrings.INTENT_REG_AS_CLIENT, allowedServices);
                regRequest.done(whatEver -> deferred.resolve(null))
                        .fail(whatEver -> deferred.reject(null));
            }));

            //val.message(serverServiceUuid, AquaStrings.INTENT_REG_AS_CLIENT, allowedServices);
        })).fail(nil -> System.err.println("AquaClientService.regAsClient"));
        return deferred;
    }


    @Override
    public void handleMessage(IPayload payload, Certificate partnerCertificate, String intent) {
        // server wants us to start simulating
        if (intent != null && intent.equals(AquaStrings.INTENT_START_SIMULATION)) {
            addJob((StartSimulationJob) payload);
        }
    }

    @Override
    public void connectionAuthenticated(Certificate partnerCertificate) {

    }

    @Override
    public void onMeinAuthIsUp() {

    }

    @Override
    protected void workWorkWork(Job job) {
        if (job instanceof SendBucketJob) {
            SendBucketJob bucketJob = (SendBucketJob) job;
            sendBucket(((SendBucketJob) job).getBucket());
        }
    }

    private void sendBucket(Bucket bucket) {
        N.r(() -> meinAuthService.connect(serverId)
                .done(validationProcess -> N.r(() ->
                        validationProcess.message(serverServiceUuid, AquaStrings.INTENT_TRANSFER_BUCKET, bucket)))
                .fail(nil ->
                        System.err.println("Eimer sagt :(")));
    }

    public Long getServerId() {
        return serverId;
    }

    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }

    public void setServerServiceUuid(String serverServiceUuid) {
        this.serverServiceUuid = serverServiceUuid;
    }
}
