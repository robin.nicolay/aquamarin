package uni.hro.networking.service;

import de.mein.auth.data.db.Service;
import de.mein.auth.data.db.ServiceType;
import de.mein.auth.service.BootLoader;
import de.mein.auth.service.MeinAuthService;
import de.mein.auth.tools.N;
import de.mein.core.serialize.exceptions.JsonDeserializationException;
import de.mein.core.serialize.exceptions.JsonSerializationException;
import de.mein.sql.SqlQueriesException;
import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * sets up an {@link AquaService}
 * Created by xor on 5/12/17.
 */
public class AquaBootLoader extends BootLoader {
    @Override
    public String getName() {
        return "aquamarin";
    }

    @Override
    public String getDescription() {
        return "fish & chips";
    }

    @Override
    public Promise<Void, Exception, Void> boot(MeinAuthService meinAuthService, List<Service> services) throws SqlQueriesException, SQLException, IOException, ClassNotFoundException, JsonDeserializationException, JsonSerializationException, IllegalAccessException {
        services.forEach(service -> N.r(() -> {
            // in case we want to persist our stuff this is the moment to load everything to restart the aquarium
        }));
        DeferredObject<Void,Exception,Void> done = new DeferredObject<>();
        done.resolve(null);
        return done;
    }


    private AquaService newInstance(String name, boolean isServer) throws SqlQueriesException {
        // database stuff comes first
        Service service = createService(name);
        // we need a directory to store lots of stuff
        File workingDirectory = new File(bootLoaderDir.getAbsolutePath() + File.separator + service.getUuid().v());
        AquaService aquaService = (isServer) ? new AquaServerService(meinAuthService, workingDirectory) : new AquaClientService(meinAuthService, workingDirectory);
        aquaService.setUuid(service.getUuid().v());
        // register the service so it can be addressed from the outside
        meinAuthService.registerMeinService(aquaService);
        // execute it. it is a worker.
        meinAuthService.execute(aquaService);
        return aquaService;
    }

    public AquaClientService newClientService(String name) throws SqlQueriesException {
        return (AquaClientService) newInstance(name, false);
    }

    public AquaServerService newServerService(String name) throws SqlQueriesException {
        return (AquaServerService) newInstance(name, true);
    }

    /**
     * write yourself into the database
     *
     * @param name
     * @return
     * @throws SqlQueriesException
     */
    private Service createService(String name) throws SqlQueriesException {
        ServiceType type = meinAuthService.getDatabaseManager().getServiceTypeByName(getName());
        Service service = meinAuthService.getDatabaseManager().createService(type.getId().v(), name);
        return service;
    }
}
