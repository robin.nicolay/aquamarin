package uni.hro.networking.job;

import de.mein.auth.jobs.Job;
import uni.hro.model.Bucket;

/**
 * Created by xor on 5/13/17.
 */
public class SendBucketJob extends Job {
    private Bucket bucket;

    public SendBucketJob(Bucket bucket) {
        this.bucket = bucket;
    }

    public Bucket getBucket() {
        return bucket;
    }
}
