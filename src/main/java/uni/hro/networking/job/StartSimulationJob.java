package uni.hro.networking.job;

import de.mein.auth.data.IPayload;
import de.mein.auth.jobs.Job;

/**
 * Created by xor on 5/15/17.
 */
public class StartSimulationJob extends Job implements IPayload {

    private int numberOfFishes, numberOfKraken, numberOfPlants, numberOfStones, width, height;
    private boolean reset;

    public StartSimulationJob(){}

    public StartSimulationJob(int numberOfFishes, int numberOfKraken, int numberOfPlants, int numberOfStones, int width, int height) {
        this.numberOfFishes = numberOfFishes;
        this.numberOfKraken = numberOfKraken;
        this.numberOfPlants = numberOfPlants;
        this.numberOfStones = numberOfStones;
        this.width = width;
        this.height = height;
    }

    public int getFishes() { return numberOfFishes; }

    public int getKraken() { return numberOfKraken; }

    public int getPlants() { return numberOfPlants; }

    public int getStones() { return numberOfStones; }

    public int getWidth() { return width; }

    public int getHeight() { return height; }
}
