package uni.hro.model;

import com.anji.integration.Activator;
import com.anji.integration.ActivatorTranscriber;
import com.anji.integration.TranscriberException;
import org.jgap.Chromosome;
import uni.hro.model.valuegenerators.*;

import java.util.*;

/**
 * Created by xor on 4/18/17.
 */
public abstract class Creature extends Consumable {

    protected float fuel;
    protected World world;
    protected Map<HitBox, Point<Integer>> hitboxes = new HashMap<>();
    protected List<HitBox> hitBoxes = new ArrayList<>();
    protected boolean looksLeft = false;
    protected List<HitBoxInputValueGenerator> inputValueGenerators = new ArrayList<>();
    protected Set<WorldEntity> collidedEntities = new HashSet<>();
    private Chromosome chromosome;
    private double speed;
    private int age = 0;
    protected boolean hasInputValueGenerators;
    private int ageAtDeath = 0;
    private boolean isHidden = false;
    private Integer inputArraySize;
    private Activator activator;

    protected void createNeuron(int offsetX, int offsetY) {
        IntegerPoint point = new IntegerPoint(offsetX, offsetY);
        HitBox neuron = new HitBox(this, point);
        hitboxes.put(neuron, point);
        hitBoxes.add(neuron);
    }


    public Chromosome getChromosome() {
        return chromosome;
    }

    protected Creature() {

    }

    public boolean isAlive() {
        return fuel > 0;
    }

    public boolean looksLeft() {
        return looksLeft;
    }

    public void setLooksLeft(boolean looksLeft) {
        this.looksLeft = looksLeft;
    }

    public Creature(World world, float fuel, float x, float y, float width, float height) {
        super(x, y, width, height);
        this.world = world;
        this.fuel = fuel;
        this.speed = 0.5;

    }

    public synchronized void addFuel(float amount) {
        this.fuel += amount;
        /*
        if (fuel > 1000) {
            this.fuel = 1000;
        }
        */
    }

    public void swim() {
        // calculates fuel with 'living cost' and current movement
        fuel = fuel - 0.06f - ((Math.abs(vx) + Math.abs(vy)) / 150);
        if (fuel < 0) {
            fuel = 0;
            die();
        }
        vx += dx;
        vy += dy;
        vx *= 0.75;
        vy *= 0.75;
        looksLeft = vx < 0;
        // To stop creatures from entering the ground: world.getHeight()-50
        // To stop creatures from leaving the window: world.getWidth()-30
        checkWorldCollision(world.getWidth() - 30, world.getHeight() - 50);
        x += vx;
        y += vy;
        dx = 0;
        dy = 0;
    }

    public void sink() {
        vx = .3f * vx;
        vy = 1;
        swim();
    }

    private boolean doesItCut(final float neuronTopY, final float neuronBottomY, final float neuronLeftX,
                              final float neuronRightX, float topY, float bottomY, float leftX, float rightX) {
        boolean isEnclosed = topY <= neuronTopY && bottomY >= neuronBottomY && leftX <= neuronLeftX
                && rightX >= neuronRightX;
        boolean isEnclosing = topY >= neuronTopY && bottomY <= neuronBottomY && leftX >= neuronLeftX
                && rightX <= neuronRightX;
        if (isEnclosed || isEnclosing)
            return true;
        int cuts = 0;
        // check top vs left and right
        if (inBetween(topY, neuronBottomY, neuronTopY)) {
            cuts += cut1(leftX, rightX, neuronRightX, neuronLeftX);
            if (cuts > 1) {
                return true;
            }
        }
        // check bottom vs left and right
        if (inBetween(bottomY, neuronBottomY, neuronTopY)) {
            cuts += cut1(leftX, rightX, neuronRightX, neuronLeftX);
            if (cuts > 1) {
                return true;
            }
        }
        // check left vs top and bottom
        if (inBetween(leftX, neuronRightX, neuronLeftX)) {
            cuts += cut1(topY, bottomY, neuronBottomY, neuronTopY);
            if (cuts > 1) {
                return true;
            }
        }
        // check right vs top and bottom
        if (inBetween(rightX, neuronRightX, neuronLeftX)) {
            cuts += cut1(topY, bottomY, neuronBottomY, neuronTopY);
            if (cuts > 1) {
                return true;
            }
        }
        return false;
    }

    public void perceive() {
        for (HitBox hitBox : hitBoxes)
            hitBox.clear();
        collidedEntities = new HashSet<>();
        float top = getTopY();
        float bottom = getBottomY();
        float left = getLeftX();
        float right = getRightX();
        for (WorldEntity entity : world.getEntities()) {
            if (entity != this) { // skip yourself ;)
                float eTop = entity.getTopY();
                float eBottom = entity.getBottomY();
                float eLeft = entity.getLeftX();
                float eRight = entity.getRightX();
                boolean collided = doesItCut(top, bottom, left, right, eTop,
                        eBottom, eLeft, eRight);
                if (collided)
                    collidedEntities.add(entity);
                for (HitBox hitBox : hitBoxes) {
                    final float neuronTopY = hitBox.getTopY();
                    final float neuronBottomY = hitBox.getBottomY();
                    final float neuronLeftX = hitBox.getLeftX();
                    final float neuronRightX = hitBox.getRightX();
                    boolean cuts = doesItCut(neuronTopY, neuronBottomY, neuronLeftX, neuronRightX, eTop,
                            eBottom, eLeft, eRight);
                    if (cuts) {
                        hitBox.addPerceivedEntity(entity);
                    }
                    if (neuronTopY < 0 || neuronBottomY > world.getHeight() || neuronLeftX < 0 || neuronRightX > world.getWidth())
                        hitBox.addPerceivedEntity(new OutOfBounds());
                }
            }
        }
    }

    private int cut1(float entityLeftX, float entityRightX, float neuronRightX, float neuronLeftX) {
        int cuts = 0;
        if (inBetween(entityLeftX, neuronRightX, neuronLeftX))
            cuts++;
        if (inBetween(entityRightX, neuronRightX, neuronLeftX))
            cuts++;
        return cuts;
    }

    private boolean inBetween(float value, float upperBound, float lowerBound) {
        return value < upperBound && value > lowerBound;
    }

    public void moveN(double factor) {
        dy += -speed * factor;
        this.checkWorldCollision(world.getWidth(), world.getHeight());
    }

    public void accelerate(double factor) {
        this.dx += looksLeft ? speed * factor * -1.0 : speed * factor;
        this.checkWorldCollision(world.getWidth(), world.getHeight());

    }

    public void moveS(double factor) {
        this.dy += speed * factor;
        this.checkWorldCollision(world.getWidth(), world.getHeight());
    }

    public void decelerate(double factor) {
        this.dx += looksLeft ? speed * factor : speed * factor * -1.0;
        this.checkWorldCollision(world.getWidth(), world.getHeight());
    }

    /**
     * call when all {@link HitBox}s have been created
     */
    public Creature createInputValueGenerators() {
        if (!hasInputValueGenerators) {
            hitboxes.forEach((inputNeuron, integerPoint) -> {
                inputValueGenerators.add(new KrakenHitBoxInputValueGenerator(inputNeuron, Kraken.class));
                inputValueGenerators.add(new FishHitBoxInputValueGenerator(inputNeuron, Fish.class));
                inputValueGenerators.add(new FoodHitBoxInputValueGenerator(inputNeuron, Food.class));
                inputValueGenerators.add(new OutOfBoundsValueGenerator(inputNeuron, OutOfBounds.class));
            });
            hasInputValueGenerators = true;
        }
        return this;
    }

    public List<HitBoxInputValueGenerator> getInputValueGenerators() {
        return inputValueGenerators;
    }

    public Map<HitBox, Point<Integer>> getHitboxes() {
        return hitboxes;
    }

    public void setFitnessValue(int age) {
        ageAtDeath = age;
        chromosome.setFitnessValue(age);
    }

    public int getFitnessValue() {
        return ageAtDeath;
    }

    public void setChromosome(Chromosome chromosome) {
        this.chromosome = chromosome;
        activator = null;
    }

    public abstract void handleCollisions();

    public void age() {
        age++;
    }

    public void onDeath() {
        chromosome.setFitnessValue(age);
    }

    public void setFuel(float fuel) {
        this.fuel = fuel;
    }

    public void reset() {
        fuel = 100f;
        consumed = false;
    }

    public int getAge() {
        return age;
    }

    public boolean isHidden() {
        return isHidden;
    }

    public void setHidden(boolean isHidden) {
        this.isHidden = isHidden;
    }

    public static Integer calcInputSize(Creature creature) {
        return creature.getInputValueGenerators().size();
    }

    public double[] createInputArray() {
        if (inputArraySize == null)
            inputArraySize = calcInputSize(this);
        return new double[inputArraySize];
    }

    public Activator getActivator(ActivatorTranscriber factory) throws TranscriberException {
        if (activator == null)
            activator = factory.newActivator(chromosome);
        return activator;
    }

    protected void die() {
        chromosome.setFitnessValue(calculateFitnessValue());
    }

    protected abstract int calculateFitnessValue();

    @Override
    public String toString() {
        return "id: " + chromosome.getId().toString();
    }
}
