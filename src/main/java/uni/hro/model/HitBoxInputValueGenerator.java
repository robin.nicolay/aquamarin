package uni.hro.model;

import uni.hro.model.valuegenerators.InputValueGenerator;

/**
 * Created by xor on 5/23/17.
 */
public abstract class HitBoxInputValueGenerator<T> implements InputValueGenerator {
    protected final HitBox hitBox;
    private final Class<T> clazz;

    public HitBoxInputValueGenerator(HitBox hitBox, Class<T> clazz) {
        this.hitBox = hitBox;
        this.clazz = clazz;
    }

    @Override
    public double getValue() {
        for (WorldEntity worldEntity : hitBox.getPerceivedEntities()) {
            if (worldEntity.getClass().equals(clazz))
                return 1.0;
        }
        return 0;
    }
}
