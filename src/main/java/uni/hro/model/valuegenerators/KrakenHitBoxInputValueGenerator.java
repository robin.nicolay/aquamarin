package uni.hro.model.valuegenerators;

import uni.hro.model.HitBox;
import uni.hro.model.HitBoxInputValueGenerator;
import uni.hro.model.Kraken;

/**
 * Created by xor on 5/23/17.
 */
public class KrakenHitBoxInputValueGenerator extends HitBoxInputValueGenerator<Kraken> {

    public KrakenHitBoxInputValueGenerator(HitBox hitBox, Class<Kraken> clazz) {
        super(hitBox, clazz);
    }
}
