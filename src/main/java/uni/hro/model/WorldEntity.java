package uni.hro.model;

import de.mein.core.serialize.SerializableEntity;
import uni.hro.draw.ColorGenerator;

import java.awt.*;


/**
 * Everything that exists in our {@link World} starts with this class.
 * Created by xor on 4/18/17.
 */
public abstract class WorldEntity implements SerializableEntity {

    // color is the general color of an object
    protected Color color;
    // currentColor is the color, that is used to draw an object
    // e.g. used to draw a fish red.
    protected Color currentColor;

    // position, velocity, delta of velocity
    protected float x, y, vx, vy, dx, dy;
    // halfWidth & halfHeight in pixels
    protected float halfWidth, halfHeight;
    // block size for the neural input in pixels
    public static final int HALF_BLOCK_SIZE = 5;
   
    
    public WorldEntity(float x, float y, float width, float height) {
        this.x = x;
        this.y = y;
        this.halfHeight = height / 2;
        this.halfWidth = width / 2;
        vx = vy = dx = dy = 0f;
        this.color = ColorGenerator.mixRandomColor(this);
        this.currentColor = color;
    }

    protected WorldEntity() {
    }

    public float getLeftX() {
        return x - halfWidth;
    }

    public float getRightX() {
        return x + halfWidth;
    }

    public float getTopY() {
        return y - halfHeight;
    }

    public float getBottomY() {
        return y + halfHeight;
    }

    public float getDx() {
        return dx;
    }

    public float getDy() {
        return dy;
    }

    public float getVx() {
        return vx;
    }

    public float getVy() {
        return vy;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getHalfWidth() {
        return halfWidth;
    }

    public float getHalfHeight() {
        return halfHeight;
    }

    public Color getCurrentColor() {
        return currentColor;
    }

    public void setCurrentColor(Color currentColor) {
        this.currentColor = currentColor;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void checkWorldCollision(int w, int h) {

        // checks if entity reached the world boundaries and bounces them off of them a little

        if (this.getRightX() >= w) {
            this.vx = -1;
        }
        if (this.getLeftX() <= 0) {
            this.vx = 1;
        }
        if (this.getTopY() <= 0) {
            this.vy = 1;
        }
        if (this.getBottomY() >= h) {
            this.vy = -1;
        }
    }


}
