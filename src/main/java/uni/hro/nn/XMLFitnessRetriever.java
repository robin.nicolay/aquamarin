/**
 * 
 */
package uni.hro.nn;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author Columbus
 *
 */
public class XMLFitnessRetriever {

	private static String max;
	private static String min;
	private static String avg;
	
	
	public static void retrieveFitness()
	{
	
		
		  try {
			  
			  
			  
			  /**
				 *  Auflisten, welche XML-Dateien sich im Pfad befinden
				 */
			  List<File> filesInFolder = Files.walk(Paths.get("nevt/fish/fitness"))
                      .filter(Files::isRegularFile)
                      .map(Path::toFile)
                      .collect(Collectors.toList());
			  
			  
			  
			  for(File filepath:  filesInFolder)
			  {
			 //System.out.println(filesInFolder);
				File fXmlFile = new File(filepath.toString());
				
				
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				
				dbFactory.setValidating(false);
				dbFactory.setNamespaceAware(true);
				dbFactory.setFeature("http://xml.org/sax/features/namespaces", false);
				dbFactory.setFeature("http://xml.org/sax/features/validation", false);
				dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
				dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
				
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(fXmlFile);
				doc.getDocumentElement().normalize();

				//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

				NodeList nList = doc.getElementsByTagName("generation");

				//System.out.println("----------------------------");

				
				/**
				 * Werte der letzten Generation auslesen.
				 */
				
			
				
				Element element = (Element) nList.item(nList.getLength()-1);
				NodeList fitnessNodes = nList.item(nList.getLength()-1).getFirstChild().getChildNodes();
				
				nList.item(nList.getLength()-1).getNextSibling();
				for (int i = 0; i < fitnessNodes.getLength(); i++) {
					
					Node nNode = nList.item(i);
					
					Element element1 = (Element) nNode;
					System.out.println("Value : " + element1.getNodeValue());
				}
				//System.out.println("neuron id : " + nList.item(nList.getLength()-1).getFirstChild());
				
				
				
				Element eElement = (Element) nList.item(nList.getLength()-1);
				
				setMax(eElement.getElementsByTagName("max").item(0).getTextContent());
				setMin(eElement.getElementsByTagName("min").item(0).getTextContent());
				setAvg(eElement.getElementsByTagName("avg").item(0).getTextContent());
		}
				
			    } catch (Exception e) {
				e.printStackTrace();
			    }
		
	//	main.setVisible(true);
	//	main.add(drawWorld);
	//	main.setSize(1000,1000);
		
		
		

	}


	public static String getMax() {
		return max;
	}


	public static void setMax(String max) {
		XMLFitnessRetriever.max = max;
	}


	public static String getMin() {
		return min;
	}


	public static void setMin(String min) {
		XMLFitnessRetriever.min = min;
	}


	public static String getAvg() {
		return avg;
	}


	public static void setAvg(String string) {
		XMLFitnessRetriever.avg = string;
	}

	
	
}
