package uni.hro.nn;

import com.anji.util.Configurable;
import com.anji.util.Properties;
import org.jgap.BulkFitnessFunction;
import org.jgap.Chromosome;

import java.util.Iterator;
import java.util.List;

public class FishFitnessFunction implements BulkFitnessFunction, Configurable {

    private int maxFitness = 0;

    @Override
    public void init(Properties props) throws Exception {
        System.out.println("FishFitnessFunction.init");
    }

    @Override
    public void evaluate(List genotypes) {
        Iterator it = genotypes.iterator();
        maxFitness = 0;
        while (it.hasNext()) {
            Chromosome c = (Chromosome) it.next();
            maxFitness = maxFitness < c.getFitnessValue() ? c.getFitnessValue() : maxFitness;
        }
    }

    @Override
    public int getMaxFitnessValue() {
        return maxFitness;
    }

}
